module Tests exposing (..)

import Expect
import Main exposing (overlaps)
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "Test suite"
        [ test "overlaps 1" <|
            \_ ->
                overlaps ( ( 0, 1 ), ( 0, 1 ) ) ( ( 2, 3 ), ( 2, 3 ) )
                    |> Expect.false ""
        , test "overlaps 2" <|
            \_ ->
                overlaps ( ( 0, 1 ), ( 0, 1 ) ) ( ( 0.9, 3 ), ( 0.9, 3 ) )
                    |> Expect.true ""
        , test "overlaps 3" <|
            \_ ->
                overlaps ( ( 0, 1 ), ( 1, 0 ) ) ( ( 0.9, 3 ), ( 3, 0.9 ) )
                    |> Expect.true ""
        ]
