import elmPlugin from "vite-plugin-elm"

export default {
    plugins: [elmPlugin({debug: false})],
    build: {
        outDir: "build",
        target: "es2020",
    }
}
