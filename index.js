import { Elm } from "./src/Main.elm"
import urlAltair from './altair.mp3'
import urlBetelgeuse from './betelgeuse.mp3'

const app = Elm.Main.init({
    node: document.getElementById("app")
})

const audioContext = new (window.AudioContext || window.webkitAudioContext)()
const gainNode = audioContext.createGain()
gainNode.connect(audioContext.destination)

let buffers = {}

function getData(id, url) {
  var request = new XMLHttpRequest();
  request.open('GET', url, true);
  request.responseType = 'arraybuffer';

  request.onload = function() {
    var audioData = request.response;

    audioContext.decodeAudioData(audioData, function(buffer) {
        buffers[id] = buffer;
        if (Object.keys(buffers).length == 2) {
            go();
        }
      },

      function(e){ console.log("Error with decoding audio data" + e.err); });
  }

  request.send();
}
getData("altair", urlAltair)
getData("betelgeuse", urlBetelgeuse)

function go() {

    function makeMusicBetelgeuse() {
        let source = audioContext.createBufferSource();
        source.loop = true;
        if (document.querySelector("#music-betelgeuse").dataset.loopend) {
            source.loopEnd = document.querySelector("#music-betelgeuse").dataset.loopend;
        }
        source.buffer = buffers["betelgeuse"]
        return source;
    }
    function makeMusicAltair() {
        let source = audioContext.createBufferSource();
        source.loop = true;
        if (document.querySelector("#music-altair").dataset.loopend) {
            source.loopEnd = document.querySelector("#music-altair").dataset.loopend;
        }
        source.buffer = buffers["altair"]
        return source;
    }

    let currentMusic = false;

    app.ports.toggleAudio.subscribe(musicOn => {
        gainNode.gain.value = musicOn;
    })

    let music;
    app.ports.switchMusic.subscribe(() => {
        if (music)music.disconnect();
        currentMusic = !currentMusic;

        if (currentMusic) {
            music = makeMusicBetelgeuse();
            music.connect(gainNode);
            music.start(0);
        } else {
            music = makeMusicAltair();
            music.connect(gainNode);
            music.start(0);
        }
    })

}