port module Main exposing (..)

import Browser
import Browser.Events
import Colors
import Html as H exposing (Html)
import Html.Attributes as HA
import Html.Events as HE
import Json.Decode as Decode
import Random exposing (Generator)
import Set exposing (Set)
import Svg as S exposing (Svg)
import Svg.Attributes as SA
import Time


type alias Model =
    { playerPosition : ( Float, Float )
    , playerSpeed : ( Float, Float )
    , playerSize : Float
    , playerDead : Bool
    , trinkets : List ( Float, Float )
    , keysPressed : Set String
    , actualCamera : Camera
    , targetCamera : Camera
    , terrainRadius : Float
    , safeZoneRadius : Float
    , home : Bool
    , pause : Bool
    , musicOn : Bool
    , debugMode : Bool
    , lastDelta : Float
    }


type Msg
    = GotRandomTrinkets (List ( Float, Float ))
    | Tick Float
    | KeyDown String
    | KeyUp String
    | KeyPress String


type alias Camera =
    { zoom : Float, cx : Float, cy : Float }


type alias SpatialAttributes =
    { x : Float, y : Float, w : Float, h : Float }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { playerPosition = ( 0, 0 )
      , playerSpeed = ( 0, 0 )
      , playerSize = 1
      , playerDead = False
      , trinkets = []
      , keysPressed = Set.empty
      , actualCamera = { zoom = 1, cx = 0, cy = 0 }
      , targetCamera = { zoom = 1, cx = 0, cy = 0 }
      , safeZoneRadius = initialTerrainRadius
      , terrainRadius = initialTerrainRadius
      , home = True
      , pause = False
      , musicOn = False
      , debugMode = False
      , lastDelta = 0
      }
    , Random.generate GotRandomTrinkets (trinketsGenerator totalTrinkets initialTerrainRadius)
    )


initialTerrainRadius : Float
initialTerrainRadius =
    20


totalTrinkets : Int
totalTrinkets =
    3


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotRandomTrinkets trinkets ->
            ( { model | trinkets = model.trinkets ++ trinkets }, Cmd.none )

        Tick delta ->
            ( { model | lastDelta = delta }
                |> updateGame delta
            , let
                remainingTrinkets =
                    List.length model.trinkets
              in
              if remainingTrinkets <= totalTrinkets then
                Random.generate GotRandomTrinkets (trinketsGenerator (totalTrinkets - remainingTrinkets) model.safeZoneRadius)

              else
                Cmd.none
            )

        KeyDown key ->
            ( { model | keysPressed = Set.insert key model.keysPressed }, Cmd.none )

        KeyUp key ->
            ( { model | keysPressed = Set.remove key model.keysPressed }, Cmd.none )

        KeyPress key ->
            case key of
                "d" ->
                    ( { model | debugMode = not model.debugMode }, Cmd.none )

                " " ->
                    if model.playerDead || model.home || model.debugMode then
                        model |> reset

                    else
                        ( model, Cmd.none )

                "m" ->
                    ( { model | musicOn = not model.musicOn }, toggleAudio (not model.musicOn) )

                "+" ->
                    ( model
                        |> (if model.debugMode then
                                changeZoom (\z -> z + 1.05)

                            else
                                identity
                           )
                    , Cmd.none
                    )

                "-" ->
                    ( model
                        |> (if model.debugMode then
                                changeZoom (\z -> z + -1.05)

                            else
                                identity
                           )
                    , Cmd.none
                    )

                "p" ->
                    ( { model | pause = not model.pause }, Cmd.none )

                _ ->
                    ( model, Cmd.none )


reset : Model -> ( Model, Cmd Msg )
reset model =
    ( { model
        | playerDead = False
        , home = False
        , playerPosition = ( 0, 0 )
        , playerSpeed = ( 0, 0 )
        , playerSize = 1
        , targetCamera = { zoom = 1, cx = 0, cy = 0 }
        , actualCamera = { zoom = 1, cx = 0, cy = 0 }
        , trinkets = []
        , safeZoneRadius = initialTerrainRadius
      }
    , switchMusic ()
    )


updateGame : Float -> Model -> Model
updateGame delta model =
    if not model.pause && not model.home then
        model
            |> updateCamera delta
            |> updateSpeed
            |> updatePosition delta
            |> reduceSafeZone
            |> checkForTrinkets model.trinkets
            |> checkForDeath

    else
        model


updateCamera : Float -> Model -> Model
updateCamera delta ({ actualCamera, targetCamera } as model) =
    if actualCamera /= targetCamera then
        let
            speed =
                0.005

            cameraRatioSpeed =
                0.00625 / actualCamera.zoom

            zoomSpeed =
                0.00333

            dx =
                targetCamera.cx - actualCamera.cx

            dy =
                targetCamera.cy - actualCamera.cy

            dZoom =
                targetCamera.zoom - actualCamera.zoom

            newX =
                if abs dx <= (actualCamera.zoom / 100) then
                    targetCamera.cx

                else if dx /= 0 then
                    actualCamera.cx + (dx * cameraRatioSpeed * delta)

                else
                    actualCamera.cx

            newY =
                if abs dy <= (actualCamera.zoom / 100) then
                    targetCamera.cy

                else if dy /= 0 then
                    actualCamera.cy + (dy * cameraRatioSpeed * delta)

                else
                    actualCamera.cy

            newZoom =
                if abs dZoom <= (actualCamera.zoom / 10) then
                    targetCamera.zoom

                else if dZoom > 0 then
                    actualCamera.zoom + (zoomSpeed * actualCamera.zoom * delta)

                else if dZoom < 0 then
                    actualCamera.zoom - (zoomSpeed * actualCamera.zoom * delta)

                else
                    actualCamera.zoom
        in
        { model
            | actualCamera =
                { zoom = newZoom
                , cx = newX
                , cy = newY
                }
        }

    else
        model


updateSpeed : Model -> Model
updateSpeed model =
    let
        keys =
            model.keysPressed

        playerSpeedAbility =
            0.01 * model.playerSize * 1.5

        xSpeed =
            case ( Set.member "ArrowLeft" keys, Set.member "ArrowRight" keys ) of
                ( True, False ) ->
                    negate playerSpeedAbility

                ( False, True ) ->
                    playerSpeedAbility

                _ ->
                    0

        ySpeed =
            case ( Set.member "ArrowDown" keys, Set.member "ArrowUp" keys ) of
                ( True, False ) ->
                    negate playerSpeedAbility

                ( False, True ) ->
                    playerSpeedAbility

                _ ->
                    0

        normalizedSpeed =
            if xSpeed /= 0 && ySpeed /= 0 then
                ( xSpeed * diagonalSpeedFactor, ySpeed * diagonalSpeedFactor )

            else
                ( xSpeed, ySpeed )
    in
    { model | playerSpeed = normalizedSpeed }


diagonalSpeedFactor : Float
diagonalSpeedFactor =
    cos (pi / 4) |> roundTo 3


updatePosition : Float -> Model -> Model
updatePosition delta model =
    if not model.playerDead then
        let
            ( oldX, oldY ) =
                model.playerPosition

            ( xSpeed, ySpeed ) =
                model.playerSpeed

            newPosition =
                ( oldX + delta * xSpeed, oldY + delta * ySpeed )

            oldTargetCamera =
                model.targetCamera

            newTargetCamera =
                { oldTargetCamera | cx = Tuple.first newPosition, cy = Tuple.second newPosition }
        in
        { model
            | playerPosition = newPosition
            , targetCamera = newTargetCamera
        }

    else
        model


checkForDeath : Model -> Model
checkForDeath model =
    if not model.playerDead then
        let
            ( x, y ) =
                model.playerPosition

            playerRadius =
                model.playerSize / 2

            xSafeMin =
                -model.safeZoneRadius + playerRadius

            ySafeMin =
                -model.safeZoneRadius + playerRadius

            xSafeMax =
                model.safeZoneRadius - playerRadius

            ySafeMax =
                model.safeZoneRadius - playerRadius
        in
        if
            (x > xSafeMax)
                || (x < xSafeMin)
                || (y > ySafeMax)
                || (y < ySafeMin)
        then
            { model | playerDead = True }

        else
            model

    else
        model


checkForTrinkets : List ( Float, Float ) -> Model -> Model
checkForTrinkets trinkets model =
    if model.playerDead then
        model

    else
        case trinkets of
            (( xTrinket, yTrinket ) as trinket) :: rest ->
                checkForTrinkets rest <|
                    if not <| pointIn trinket (radiusToArea model.safeZoneRadius) then
                        model |> removeTrinket trinket

                    else
                        let
                            ( x, y ) =
                                model.playerPosition

                            playerRadius =
                                model.playerSize / 2

                            d =
                                sqrt ((xTrinket - x) ^ 2 + (yTrinket - y) ^ 2)
                        in
                        if d < playerRadius * 1.5 then
                            model
                                |> removeTrinket trinket
                                |> shrinkPlayer

                        else
                            model

            _ ->
                model


removeTrinket : ( Float, Float ) -> Model -> Model
removeTrinket trinket model =
    { model | trinkets = List.filter (\t -> t /= trinket) model.trinkets }


pointIn : ( Float, Float ) -> ( ( Float, Float ), ( Float, Float ) ) -> Bool
pointIn (( x, y ) as point) (( ( x1, x2 ), ( y1, y2 ) ) as area) =
    x <= x2 && x >= x1 && y <= y2 && y >= y1


radiusToArea : Float -> ( ( Float, Float ), ( Float, Float ) )
radiusToArea radius =
    ( ( -radius, radius ), ( -radius, radius ) )


reduceSafeZone : Model -> Model
reduceSafeZone model =
    if not model.playerDead then
        { model | safeZoneRadius = model.safeZoneRadius - 0.01 }

    else
        model


shrinkPlayer : Model -> Model
shrinkPlayer model =
    let
        camera =
            model.targetCamera

        newCamera =
            { camera
                | zoom = camera.zoom * 2
                , cx = Tuple.first model.playerPosition
                , cy = Tuple.second model.playerPosition
            }
    in
    { model
        | playerSize = model.playerSize / 2
        , targetCamera = newCamera
    }


view : Model -> Html Msg
view model =
    H.div
        [ HA.style "width" "100vw"
        , HA.style "height" "100vh"
        , HA.style "box-sizing" "border-box"
        , HA.style "display" "flex"
        , HA.style "justify-content" "center"
        , HA.style "align-items" "center"
        ]
        [ S.svg
            [ HA.style "border" "1px solid #ccc"
            , HA.style "width" "1px solid #ccc"
            , HA.style "aspect-ratio" "16/9"
            , HA.style "max-height" "90vh"
            , SA.viewBox <| String.join " " (List.map String.fromFloat [ 0, 0, sceneWidth, sceneHeight ])
            , HA.style "background-color" Colors.background
            ]
            (drawGame model)
        , H.audio [ HA.id "music-altair", HA.src "altair.mp3", HA.attribute "data-loopend" "14.73" ] []
        , H.audio [ HA.id "music-betelgeuse", HA.src "betelgeuse.mp3" ] []
        ]


sceneWidth : Float
sceneWidth =
    320


sceneHeight : Float
sceneHeight =
    180


drawGame : Model -> List (Svg Msg)
drawGame ({ terrainRadius, actualCamera, safeZoneRadius, playerPosition, playerSize, trinkets, debugMode } as model) =
    let
        wholeTerrain =
            ( ( -terrainRadius, terrainRadius ), ( -terrainRadius, terrainRadius ) )

        viewport =
            viewportToWorld actualCamera

        visibleTiles =
            visibleTerrainTiles viewport wholeTerrain
    in
    [ tileSymbol
    , S.g [] (List.map (\tile -> drawTerrain tile actualCamera) visibleTiles)
    , drawSafeZone safeZoneRadius actualCamera
    , drawTrinkets trinkets actualCamera
    , drawPlayer playerPosition playerSize actualCamera
    , drawDangerHints (computeDangerHints model)
    , if model.home then
        drawHomeScreen

      else if model.playerDead then
        drawDeathScreen

      else
        nothing
    , drawOverlay
    ]
        ++ (if debugMode then
                [ drawDebugInfo model (String.join ", " (List.map tileToString visibleTiles)) ]

            else
                []
           )


tileToString : ( ( Float, Float ), ( Float, Float ) ) -> String
tileToString ( ( x1, x2 ), ( y1, y2 ) ) =
    "( "
        ++ String.fromFloat x1
        ++ " "
        ++ String.fromFloat
            x2
        ++ " | "
        ++ String.fromFloat
            y1
        ++ " "
        ++ String.fromFloat
            y2
        ++ " )"


drawPlayer : ( Float, Float ) -> Float -> Camera -> Svg Msg
drawPlayer ( x, y ) size camera =
    S.rect
        ([ SA.fill Colors.player
         , SA.style <|
            "filter: drop-shadow(0 0 1px currentColor) drop-shadow(0 0 10px currentColor);"
                ++ "animation: player .25s linear infinite alternate;"
         , HA.attribute "transform-origin" (String.fromFloat (xToScreen camera x) ++ " " ++ String.fromFloat (yToScreen True camera y))
         ]
            ++ ({ x = x, y = y, w = size, h = size }
                    |> center
                    |> toScreen True camera
                    |> toSvgAttrs
               )
        )
        []


drawSafeZone : Float -> Camera -> Svg Msg
drawSafeZone radius camera =
    S.g
        []
        [ S.rect
            ([ SA.stroke Colors.limit, SA.fill "none", SA.strokeWidth (String.fromFloat <| lengthToScreen camera 0.1) ]
                ++ ({ x = 0, y = 0, w = 2 * radius, h = 2 * radius }
                        |> center
                        |> toScreen True camera
                        |> toSvgAttrs
                   )
            )
            []
        ]


drawTrinkets : List ( Float, Float ) -> Camera -> Svg Msg
drawTrinkets trinkets camera =
    S.g [] (List.map (\t -> drawTrinket t camera) trinkets)


drawTrinket : ( Float, Float ) -> Camera -> Svg Msg
drawTrinket ( x, y ) camera =
    S.rect
        [ SA.fill Colors.trinket
        , SA.width "4"
        , SA.height "4"
        , SA.x (String.fromFloat (xToScreen camera x - 2))
        , SA.y (String.fromFloat (yToScreen True camera y - 2))
        , SA.style <|
            "animation: trinket .5s linear infinite;"
                ++ "filter: drop-shadow(0 0 1px "
                ++ Colors.trinket
                ++ ") "
                ++ "drop-shadow(0 0 5px "
                ++ Colors.trinket
                ++ ")"
        , HA.attribute "transform-origin" (String.fromFloat (xToScreen camera x) ++ " " ++ String.fromFloat (yToScreen True camera y))
        ]
        []


computeDangerHints : Model -> { top : Float, right : Float, bottom : Float, left : Float }
computeDangerHints model =
    let
        ( x, y ) =
            model.playerPosition

        ( topWall, rightWall ) =
            ( model.safeZoneRadius, model.safeZoneRadius )

        ( bottomWall, leftWall ) =
            ( -model.safeZoneRadius, -model.safeZoneRadius )

        dTop =
            topWall - y

        dBottom =
            y - bottomWall

        dLeft =
            x - leftWall

        dRight =
            rightWall - x

        distanceToDangerHint d =
            (d - 10)
                / (2 - 10)
                |> Basics.clamp 0 1
    in
    { top = distanceToDangerHint dTop
    , right = distanceToDangerHint dRight
    , bottom = distanceToDangerHint dBottom
    , left = distanceToDangerHint dLeft
    }


drawDangerHints : { top : Float, right : Float, bottom : Float, left : Float } -> Svg Msg
drawDangerHints danger =
    S.g
        []
        [ S.linearGradient
            [ SA.id "danger-hint-top"
            , SA.x1 "0"
            , SA.x2 "0"
            , SA.y1 "0"
            , SA.y2 "1"
            ]
            [ S.stop [ SA.offset "0%", SA.stopColor "#fff", SA.stopOpacity "0.5" ] []
            , S.stop [ SA.offset "100%", SA.stopColor "#fff", SA.stopOpacity "0" ] []
            ]
        , S.rect
            [ SA.x "0"
            , SA.y "0"
            , SA.width (String.fromFloat sceneWidth)
            , SA.height (String.fromFloat (sceneHeight / 10))
            , SA.fill "url(#danger-hint-top)"
            , SA.opacity (String.fromFloat danger.top)
            ]
            []
        , S.linearGradient
            [ SA.id "danger-hint-bottom"
            , SA.x1 "0"
            , SA.x2 "0"
            , SA.y1 "1"
            , SA.y2 "0"
            ]
            [ S.stop [ SA.offset "0%", SA.stopColor "#fff", SA.stopOpacity "0.5" ] []
            , S.stop [ SA.offset "100%", SA.stopColor "#fff", SA.stopOpacity "0" ] []
            ]
        , S.rect
            [ SA.x "0"
            , SA.y (String.fromFloat (sceneHeight - (sceneHeight / 10)))
            , SA.width (String.fromFloat sceneWidth)
            , SA.height (String.fromFloat (sceneHeight / 10))
            , SA.fill "url(#danger-hint-bottom)"
            , SA.opacity (String.fromFloat danger.bottom)
            ]
            []
        , S.linearGradient
            [ SA.id "danger-hint-left"
            , SA.x1 "0"
            , SA.x2 "1"
            , SA.y1 "0"
            , SA.y2 "0"
            ]
            [ S.stop [ SA.offset "0%", SA.stopColor "#fff", SA.stopOpacity "0.5" ] []
            , S.stop [ SA.offset "100%", SA.stopColor "#fff", SA.stopOpacity "0" ] []
            ]
        , S.rect
            [ SA.x "0"
            , SA.y "0"
            , SA.height (String.fromFloat sceneHeight)
            , SA.width (String.fromFloat (sceneWidth / 10))
            , SA.fill "url(#danger-hint-left)"
            , SA.opacity (String.fromFloat danger.left)
            ]
            []
        , S.linearGradient
            [ SA.id "danger-hint-right"
            , SA.x1 "1"
            , SA.x2 "0"
            , SA.y1 "0"
            , SA.y2 "0"
            ]
            [ S.stop [ SA.offset "0%", SA.stopColor "#fff", SA.stopOpacity "0.5" ] []
            , S.stop [ SA.offset "100%", SA.stopColor "#fff", SA.stopOpacity "0" ] []
            ]
        , S.rect
            [ SA.x (String.fromFloat (sceneWidth - sceneWidth / 10))
            , SA.y "0"
            , SA.height (String.fromFloat sceneHeight)
            , SA.width (String.fromFloat (sceneWidth / 10))
            , SA.fill "url(#danger-hint-right)"
            , SA.opacity (String.fromFloat danger.right)
            ]
            []
        ]


drawHomeScreen : Svg Msg
drawHomeScreen =
    S.g
        []
        [ S.rect
            [ SA.x "0"
            , SA.y "0"
            , SA.width (String.fromFloat sceneWidth)
            , SA.height (String.fromFloat sceneHeight)
            , SA.fill "rgba(0,0,0,0.75)"
            ]
            []
        , S.text_
            [ SA.fill Colors.trinket
            , SA.fontFamily "monospace"
            , SA.fontWeight "bold"
            , SA.x (String.fromFloat <| middle 0 sceneWidth)
            , SA.y (String.fromFloat <| middle 0 sceneHeight)
            , SA.textAnchor "middle"
            , SA.dominantBaseline "middle"
            ]
            [ S.text "Welcome." ]

        -- “Death is the irreversible cessation of all biological functions that sustain an organism.
        -- Death is an inevitable, universal process that eventually occurs in all organisms.”
        -- – Wikipedia
        , S.text_
            [ SA.fill Colors.trinket
            , SA.fontFamily "monospace"
            , SA.fontWeight "bold"
            , SA.fontSize "7"
            , SA.x (String.fromFloat <| middle 0 sceneWidth)
            , SA.y (String.fromFloat <| pointAt 0.8 0 sceneHeight)
            , SA.textAnchor "middle"
            , SA.dominantBaseline "middle"
            ]
            [ S.text "Press Space to start, Arrow keys to move, P to pause, M to mute" ]
        ]


drawDeathScreen : Svg Msg
drawDeathScreen =
    S.g
        []
        [ S.rect
            [ SA.x "0"
            , SA.y "0"
            , SA.width (String.fromFloat sceneWidth)
            , SA.height (String.fromFloat sceneHeight)
            , SA.fill "rgba(0,0,0,0.75)"
            ]
            []
        , S.text_
            [ SA.fill "red"
            , SA.fontFamily "monospace"
            , SA.fontWeight "bold"
            , SA.x (String.fromFloat <| middle 0 sceneWidth)
            , SA.y (String.fromFloat <| middle 0 sceneHeight)
            , SA.textAnchor "middle"
            , SA.dominantBaseline "middle"
            ]
            [ S.text "OUCH!™" ]

        -- “Death is the irreversible cessation of all biological functions that sustain an organism.
        -- Death is an inevitable, universal process that eventually occurs in all organisms.”
        -- – Wikipedia
        , S.text_
            [ SA.fill "red"
            , SA.fontFamily "monospace"
            , SA.fontWeight "bold"
            , SA.fontSize "7"
            , SA.x (String.fromFloat <| middle 0 sceneWidth)
            , SA.y (String.fromFloat <| pointAt 0.8 0 sceneHeight)
            , SA.textAnchor "middle"
            , SA.dominantBaseline "middle"
            ]
            [ S.text "Press Space to try again" ]
        ]


tileSymbol : Svg Msg
tileSymbol =
    S.symbol [ SA.id "tile", SA.viewBox "0 0 10 10", SA.width "10", SA.height "10" ]
        [ S.rect
            [ SA.fill Colors.decor
            , SA.x "4.75"
            , SA.y "4.75"
            , SA.width ".5"
            , SA.height ".5"
            ]
            []
        ]


drawTerrain : ( ( Float, Float ), ( Float, Float ) ) -> Camera -> Svg Msg
drawTerrain area camera =
    let
        iterateTerrainForArea iterations ( ( x1, x2 ), ( y1, y2 ) ) =
            if iterations <= 0 then
                []

            else
                [ S.use
                    [ SA.xlinkHref "#tile"
                    , SA.x (String.fromFloat (xToScreen camera x1))
                    , SA.y (String.fromFloat (yToScreen True camera y2))
                    , SA.width (String.fromFloat (lengthToScreen camera (x2 - x1)))
                    , SA.height (String.fromFloat (lengthToScreen camera (y2 - y1)))
                    ]
                    []
                ]
                    ++ List.concatMap
                        (iterateTerrainForArea (iterations - 1))
                        (divideArea ( ( x1, x2 ), ( y1, y2 ) ))

        iterationsFromZoomLevel : Float -> Float
        iterationsFromZoomLevel zoom =
            4 + logBase 2 zoom
    in
    S.g [] (iterateTerrainForArea 3 area)


divideArea : ( ( Float, Float ), ( Float, Float ) ) -> List ( ( Float, Float ), ( Float, Float ) )
divideArea ( ( x1, x2 ), ( y1, y2 ) ) =
    [ ( ( x1, middle x1 x2 ), ( middle y1 y2, y2 ) )
    , ( ( middle x1 x2, x2 ), ( middle y1 y2, y2 ) )
    , ( ( middle x1 x2, x2 ), ( y1, middle y1 y2 ) )
    , ( ( x1, middle x1 x2 ), ( y1, middle y1 y2 ) )
    ]


middle : Float -> Float -> Float
middle x1 x2 =
    pointAt 0.5 x1 x2


pointAt : Float -> Float -> Float -> Float
pointAt at x1 x2 =
    x1 + ((x2 - x1) * at)


visibleTerrainTiles :
    ( ( Float, Float ), ( Float, Float ) )
    -> ( ( Float, Float ), ( Float, Float ) )
    -> List ( ( Float, Float ), ( Float, Float ) )
visibleTerrainTiles viewport area =
    if not <| overlaps area viewport then
        []

    else
        case
            List.filter
                (\subArea -> overlaps subArea viewport)
                (divideArea area)
        of
            [ singleArea ] ->
                -- A single quarter is visible, we can go deeper
                visibleTerrainTiles viewport singleArea

            subAreas ->
                subAreas


spatialAttributesToArea : SpatialAttributes -> ( ( Float, Float ), ( Float, Float ) )
spatialAttributesToArea { x, y, w, h } =
    ( ( x, x + w ), ( y, y + h ) )


areaToSpatialAttributes : ( ( Float, Float ), ( Float, Float ) ) -> SpatialAttributes
areaToSpatialAttributes ( ( x1, x2 ), ( y1, y2 ) ) =
    { x = x1, y = y1, w = x2 - x1, h = y2 - y1 }


overlaps : ( ( Float, Float ), ( Float, Float ) ) -> ( ( Float, Float ), ( Float, Float ) ) -> Bool
overlaps ( ( x1, x2 ), ( y1, y2 ) ) ( ( x1_, x2_ ), ( y1_, y2_ ) ) =
    x1 <= x2_ && x2 >= x1_ && y1 <= y2_ && y2 >= y1_


drawOverlay : Svg Msg
drawOverlay =
    S.g []
        [ S.radialGradient [ SA.id "overlay-gradient" ]
            [ S.stop [ SA.offset "40%", SA.stopColor "transparent" ] []
            , S.stop [ SA.offset "100%", SA.stopColor "rgb(0,0,0,0.9)" ] []
            ]
        , S.rect
            [ SA.x "0"
            , SA.y "0"
            , SA.width (String.fromFloat sceneWidth)
            , SA.height (String.fromFloat sceneHeight)
            , SA.fill "url(#overlay-gradient)"
            , SA.transform "scale(1.85)"
            , HA.attribute "transform-origin" "center"
            ]
            []
        ]


{-| (Debug function)
-}
changeZoom : (Float -> Float) -> Model -> Model
changeZoom fn ({ actualCamera } as model) =
    { model
        | actualCamera = { actualCamera | zoom = fn actualCamera.zoom }
        , targetCamera = { actualCamera | zoom = fn actualCamera.zoom }
    }


drawDebugInfo : Model -> String -> Svg Msg
drawDebugInfo model terrainInfo =
    let
        ( x, y ) =
            model.playerPosition
                |> Tuple.mapBoth (roundTo 3) (roundTo 3)

        ( xSpeed, ySpeed ) =
            model.playerSpeed

        { zoom, cx, cy } =
            model.actualCamera
    in
    [ "Player position: " ++ String.fromFloat x ++ " " ++ String.fromFloat y
    , "Player speed: " ++ String.fromFloat xSpeed ++ " " ++ String.fromFloat ySpeed
    , "Safe zone radius: " ++ String.fromFloat (model.safeZoneRadius |> roundTo 3)
    , "Camera: x" ++ String.fromFloat (zoom |> roundTo 3) ++ " (" ++ String.fromFloat (cx |> roundTo 3) ++ ", " ++ String.fromFloat (cy |> roundTo 3) ++ ")"
    , "Target camera: x" ++ String.fromFloat (model.targetCamera.zoom |> roundTo 3) ++ " (" ++ String.fromFloat (model.targetCamera.cx |> roundTo 3) ++ ", " ++ String.fromFloat (model.targetCamera.cy |> roundTo 3) ++ ")"
    , "FPS: " ++ String.fromInt (1000 / model.lastDelta |> round)
    , "Viewport coordinates: " ++ tileToString (viewportToWorld model.actualCamera)
    , "Terrain: " ++ terrainInfo
    ]
        |> List.indexedMap
            (\i line ->
                S.text_
                    [ SA.x (String.fromFloat 5)
                    , SA.y (String.fromFloat (sceneHeight / 2 + (toFloat i * 6)))
                    , SA.fontSize "5"
                    , SA.fill Colors.text
                    ]
                    [ S.text line ]
            )
        |> S.g []


trinketsGenerator : Int -> Float -> Generator (List ( Float, Float ))
trinketsGenerator count safeZoneRadius =
    Random.list count (trinketGenerator safeZoneRadius)


trinketGenerator : Float -> Generator ( Float, Float )
trinketGenerator safeZoneRadius =
    Random.map2
        Tuple.pair
        (Random.float -safeZoneRadius safeZoneRadius)
        (Random.float -safeZoneRadius safeZoneRadius)


roundTo : Int -> Float -> Float
roundTo decimals value =
    value
        |> (*) (10 ^ toFloat decimals)
        |> round
        |> toFloat
        |> (\v -> v / (10 ^ toFloat decimals))


center : SpatialAttributes -> SpatialAttributes
center { x, y, w, h } =
    { x = x - w / 2, y = y + h / 2, w = w, h = h }


constantZoomFactor : Float
constantZoomFactor =
    10


toScreen : Bool -> Camera -> SpatialAttributes -> SpatialAttributes
toScreen flipY ({ zoom, cx, cy } as camera) { x, y, w, h } =
    let
        offsetX =
            sceneWidth / 2

        offsetY =
            sceneHeight / 2
    in
    { x = xToScreen camera x
    , y = yToScreen flipY camera y
    , w = lengthToScreen camera w
    , h = lengthToScreen camera h
    }


viewportToWorld : Camera -> ( ( Float, Float ), ( Float, Float ) )
viewportToWorld { zoom, cx, cy } =
    let
        offsetX =
            (sceneWidth / 2) / (zoom * constantZoomFactor)

        offsetY =
            (sceneHeight / 2) / (zoom * constantZoomFactor)
    in
    ( ( cx - offsetX, cx + offsetX )
    , ( cy - offsetY, cy + offsetY )
    )


xToScreen : Camera -> Float -> Float
xToScreen { zoom, cx, cy } x =
    let
        offsetX =
            sceneWidth / 2
    in
    (x - cx) * (zoom * constantZoomFactor) + offsetX


yToScreen : Bool -> Camera -> Float -> Float
yToScreen flip { zoom, cx, cy } y =
    let
        offsetY =
            sceneHeight / 2
    in
    ((y - cy)
        * (zoom * constantZoomFactor)
        |> (if flip then
                negate

            else
                identity
           )
    )
        + offsetY


lengthToScreen : Camera -> Float -> Float
lengthToScreen { zoom } l =
    l * zoom * constantZoomFactor


toSvgAttrs : SpatialAttributes -> List (S.Attribute Msg)
toSvgAttrs { x, y, w, h } =
    [ SA.width (String.fromFloat w)
    , SA.height (String.fromFloat h)
    , SA.x (String.fromFloat x)
    , SA.y (String.fromFloat y)
    ]


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Browser.Events.onAnimationFrameDelta Tick
        , Browser.Events.onKeyDown (Decode.field "key" Decode.string |> Decode.map KeyDown)
        , Browser.Events.onKeyUp (Decode.field "key" Decode.string |> Decode.map KeyUp)
        , Browser.Events.onKeyPress (Decode.field "key" Decode.string |> Decode.map KeyPress)
        ]


nothing : Svg msg
nothing =
    S.g [] []


port toggleAudio : Bool -> Cmd msg


port switchMusic : () -> Cmd msg


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }
