module Colors exposing (..)


background : String
background =
    "#0a0324"


player : String
player =
    "#fff"


limit : String
limit =
    "#fff"


decor : String
decor =
    "hsl(252.7, 40%, 27.6%)"


text : String
text =
    player


trinket : String
trinket =
    "#33ff00"
